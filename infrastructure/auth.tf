resource "azurerm_resource_group" "group" {
  name     = module.this.id
  location = var.azure_location
}