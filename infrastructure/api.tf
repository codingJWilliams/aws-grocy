resource "aws_apigatewayv2_api" "fava" {
  name          = "${module.this.id}-fava"
  protocol_type = "HTTP"
}
resource "aws_apigatewayv2_integration" "fava" {
  api_id           = aws_apigatewayv2_api.fava.id
  integration_type = "AWS_PROXY"

  connection_type = "INTERNET"

  #content_handling_strategy = "CONVERT_TO_TEXT"
  description          = "Lambda example"
  integration_method   = "POST"
  integration_uri      = aws_lambda_function.fava.invoke_arn
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_route" "fava" {
  api_id    = aws_apigatewayv2_api.fava.id
  route_key = "ANY /{proxy+}"

  target = "integrations/${aws_apigatewayv2_integration.fava.id}"
}

resource "aws_apigatewayv2_stage" "prod" {
  api_id      = aws_apigatewayv2_api.fava.id
  name        = "example-stage"
  auto_deploy = true
}

resource "aws_apigatewayv2_domain_name" "domain" {
  domain_name = "${var.subdomain}.${var.domain}"

  domain_name_configuration {
    certificate_arn = aws_acm_certificate.domain-eu.arn
    endpoint_type   = "REGIONAL"
    security_policy = "TLS_1_2"
  }
}

resource "aws_apigatewayv2_api_mapping" "domain" {
  api_id      = aws_apigatewayv2_api.fava.id
  domain_name = aws_apigatewayv2_domain_name.domain.id
  stage       = aws_apigatewayv2_stage.prod.id
}