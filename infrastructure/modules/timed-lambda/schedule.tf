# resource "aws_iam_role" "scheduler" {
#   name                = "${module.this.id}-scheduler"
#   managed_policy_arns = [aws_iam_policy.scheduler.arn]
#   assume_role_policy  = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": "sts:AssumeRole",
#       "Principal": {
#         "Service": "scheduler.amazonaws.com"
#       },
#       "Effect": "Allow",
#       "Sid": ""
#     }
#   ]
# }
# EOF
# }

# resource "aws_iam_policy" "scheduler" {
#   name        = "${module.this.id}-scheduler"
#   description = "Allow EventBridge to call the Lambda"
#   policy      = data.aws_iam_policy_document.scheduler.json
# }
# data "aws_iam_policy_document" "scheduler" {
#   version = "2012-10-17"
#   statement {
#     actions = [
#       "lambda:InvokeFunction",
#     ]
#     effect = "Allow"
#     resources = [module.function.arn]
#   }
#   statement {
#     actions = [
#       "iam:PassRole",
#     ]
#     effect    = "Allow"
#     resources = [aws_iam_role.function.arn]
#   }
# }

resource "aws_cloudwatch_event_rule" "schedule" {
  name                = module.this.id
  description         = "Run scheduled lambda"
  schedule_expression = "cron(${var.schedule})"
  is_enabled = false
}

resource "aws_cloudwatch_event_target" "schedule" {
  rule      = aws_cloudwatch_event_rule.schedule.name
  target_id = "lambda"
  arn       = module.function.arn
}
