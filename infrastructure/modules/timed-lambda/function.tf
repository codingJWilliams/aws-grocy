resource "local_file" "function" {
  filename = "${var.src}/params.json"
  content = jsonencode(var.params)
}

data "archive_file" "function" {
  type        = "zip"
  source_dir  = var.src
  output_path = "${path.module}/function.zip"
  depends_on = [
    local_file.function
  ]
  excludes = [
    "__pycache__",
    "venv",
  ]
}

resource "aws_iam_role" "function" {
  name                = "${module.this.id}-lambda"
  managed_policy_arns = [aws_iam_policy.function.arn, "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]

  assume_role_policy = data.aws_iam_policy_document.AssumeByLambda.json
}

data "aws_iam_policy_document" "AssumeByLambda" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "function" {
  name        = "${module.this.id}-lambda"
  description = "Permissions for the lambda itself"

  policy = var.function_policy
}

resource "aws_lambda_permission" "function" {
  action        = "lambda:InvokeFunction"
  function_name = module.function.name
  principal     = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
}

data "aws_caller_identity" "current" {}

resource "aws_lambda_permission" "function2" {
  action        = "lambda:InvokeFunction"
  function_name = module.function.name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.schedule.arn
}

module "function" {
  source  = "terraform-module/lambda/aws"
  version = "~> 2"

  function_name      = module.this.id
  filename            = data.archive_file.function.output_path
  source_code_hash  = "${data.archive_file.function.output_base64sha256}"
  description        = module.this.id
  handler            = var.lambda.handler
  runtime            = var.lambda.runtime
  memory_size        = "128"
  concurrency        = -1
  lambda_timeout     = "60"
  log_retention      = "1"
  role_arn           = aws_iam_role.function.arn

  vpc_config = var.lambda.vpc_config
  file_system_config = var.lambda.file_system_config
}