variable "name" {
  type        = string
  description = "Name for resources in module"
}

variable "context" {
  type        = any
  description = "Naming context for resources in module"
}

variable "schedule" {
  type        = string
  description = "Cron schedule to execute lambda on"
}

variable "src" {
  type        = string
  description = "Path to source code for lambda"
}

variable "lambda" {
  type = object({
    handler            = string
    runtime            = string
    vpc_config         = any
    file_system_config = any
  })
  description = "Path to source code for lambda"
}

variable "params" {
  type = any
  description = "JSON to be passed to function"
}

variable "function_policy" {
  type = string
  description = "JSON policy object for the lambda itself"
}