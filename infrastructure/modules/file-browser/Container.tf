resource "aws_ecs_cluster" "cluster" {
  name = var.name
  tags = {
    "FileBrowser" = var.name
  }
}

resource "aws_ecs_cluster_capacity_providers" "providers" {
  cluster_name = aws_ecs_cluster.cluster.name

  capacity_providers = ["FARGATE", "FARGATE_SPOT"]

  default_capacity_provider_strategy {
    base              = 1
    weight            = 100
    capacity_provider = "FARGATE"
  }
}

resource "aws_iam_role" "taskrole" {
  name                = var.name
  managed_policy_arns = [aws_iam_policy.taskrole.arn]
  assume_role_policy  = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    "FileBrowser" = var.name
  }
}

resource "aws_iam_policy" "taskrole" {
  name        = var.name
  description = "Allows ECS to upload logs for backup jobs"
  policy      = data.aws_iam_policy_document.taskrole.json

  tags = {
    "FileBrowser" = var.name
  }
}
data "aws_iam_policy_document" "taskrole" {
  version = "2012-10-17"
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogStreams"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:logs:*:*:*"
    ]
  }

  statement {
    actions = [
      "elasticfilesystem:ClientWrite",
      "elasticfilesystem:ClientMount"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:elasticfilesystem:eu-west-2:550723713300:file-system/fs-0f1cf8e60ef4360fa"
    ]
  }
}

resource "aws_cloudwatch_log_group" "yada" {
  name = "${var.name}-sync"

  tags = {
    "FileBrowser" = var.name
  }
}

resource "aws_ecs_task_definition" "service" {
  family = "${var.name}-sync"

  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256 # 0.25vcpu
  memory                   = 512
  execution_role_arn       = aws_iam_role.taskrole.arn
  task_role_arn            = aws_iam_role.taskrole.arn

  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }

  volume {
    name = "example"

    efs_volume_configuration {
      file_system_id = "fs-0f1cf8e60ef4360fa"
    }
  }

  container_definitions = jsonencode([
    {
      name      = "filemanager"
      image     = "tinyfilemanager/tinyfilemanager:master"
      essential = true
      cpu       = 10
      memory    = 256
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]

      logConfiguration = {
        logDriver = "awslogs",
        options = {
          awslogs-group         = aws_cloudwatch_log_group.yada.name,
          awslogs-region        = "eu-west-2",
          awslogs-stream-prefix = "ecs"
        }
      },
      mountPoints = [
        {
          sourceVolume  = "example",
          containerPath = "/var/www/html/efs"
        }
      ],
      environment = [
        # {
        #   name  = "AZURE_STORAGE_ACCOUNT"
        #   value = azurerm_storage_account.destination.name
        # }
      ]
    }
  ])

  tags = {
    "FileBrowser" = var.name
  }
}


resource "aws_ecs_service" "browser" {
  name                  = "filebrowser-${var.name}"
  cluster               = aws_ecs_cluster.cluster.id
  task_definition       = aws_ecs_task_definition.service.arn
  desired_count         = 1
  wait_for_steady_state = true
  # iam_role        = aws_iam_role.taskrole.arn
  # depends_on      = [aws_iam_role_policy.taskrole]

  network_configuration {
    subnets          = ["subnet-021c5bf0b43f00ec1"]
    security_groups  = ["sg-03119ae42a2e80084"]
    assign_public_ip = true
  }

  # ordered_placement_strategy {
  #   type  = "binpack"
  #   field = "cpu"
  # }

  # load_balancer {
  #   target_group_arn = aws_lb_target_group.foo.arn
  #   container_name   = "mongo"
  #   container_port   = 8080
  # }

  # placement_constraints {
  #   type       = "memberOf"
  #   expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b]"
  # }
}
