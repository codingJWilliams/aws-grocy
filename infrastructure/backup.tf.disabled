module "backup" {
  source = "./modules/timed-lambda"

  src      = "${path.module}/../lambda/backup"
  name     = "backup-ledger"
  context  = module.this
  schedule = "34 3 * * ? *"
  lambda = {
    handler = "handler.handler"
    runtime = "python3.9"
    vpc_config = {
      subnet_ids         = [data.aws_subnet.default.id]
      security_group_ids = [aws_security_group.default.id]
    }
    file_system_config = {
      efs_access_point_arn = aws_efs_access_point.fava.arn
      local_mount_path     = "/mnt/efs"
    }
  }

  params = {
    ledger_bucket = aws_s3_bucket.ledger.bucket
  }

  function_policy = data.aws_iam_policy_document.backup.json
}

data "aws_iam_policy_document" "backup" {
  statement {
    actions = [
      "s3:PutObject"
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.ledger.arn}/ledger.beancount"]
  }
}

resource "aws_s3_bucket" "ledger" {
  bucket = "${module.this.id}-ledger"
}

resource "aws_s3_bucket_versioning" "ledger" {
  bucket = aws_s3_bucket.ledger.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "ledger" {
  bucket = aws_s3_bucket.ledger.id

  rule {
    id     = "delete-old"
    status = "Enabled"
    expiration {
      days = 14
    }
  }
}

resource "aws_s3_bucket_acl" "ledger" {
  bucket = aws_s3_bucket.ledger.bucket
  acl    = "private"
}

resource "aws_s3_bucket_public_access_block" "ledger" {
  bucket = aws_s3_bucket.ledger.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
