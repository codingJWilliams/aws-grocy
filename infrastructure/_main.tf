provider "aws" {
  region  = var.region
  profile = var.profile

  default_tags {
    tags = {
      service = "grocy"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  profile = var.profile
  alias   = "us-east-1"

  default_tags {
    tags = {
      service = "grocy"
    }
  }
}

provider "azurerm" {
  features {}
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.59.0"
    }

    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }

  }

  backend "s3" {
    bucket  = "g8-terraform-state"
    key     = "grocy/terraform.tfstate"
    region  = "eu-west-2"
    encrypt = false
    profile = "jw-personal"
  }
}

module "this" {
  source = "cloudposse/label/null"

  namespace   = "g8"
  environment = var.environment
  stage       = "grocy"

  label_order = ["namespace", "stage", "environment", "name"]
}